public static bool Equals(byte[] array1, byte[] array2) {
	if(array1.Length != array2.Length)
		return false;
	
	int len = array1.Length;
	
	unsafe {
		fixed(byte* ap = array1, bp = array2) {
			long* alp = (long*)ap, blp = (long*)bp;
			
			for(; len >= 8; len -= 8) {
				if(*alp != *blp)
					return false;
				
				alp++;
				blp++;
			}
			
			byte* ap2 = (byte*)alp, bp2 = (byte*)blp;
			
			for(; len > 0; len--) {
				if(*ap2 != *bp2)
					return false;
				
				ap2++;
				bp2++;
			}
		}
	}
	
	return true;
}