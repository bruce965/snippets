﻿using System;
using System.IO;

namespace Utilities
{
	public class StreamWrapper : Stream
	{
		private readonly Stream stream;
		private readonly bool read;
		private readonly bool write;
		private readonly bool seek;
		
		public StreamWrapper(Stream stream, bool read, bool write, bool seek) {
			this.stream = stream;
			this.read = read;
			this.write = write;
			this.seek = seek;
			
			if(read && !stream.CanRead)
				throw new ArgumentException("Cannot make stream readable as base stream is not readable.", "read");
			
			if(write && !stream.CanWrite)
				throw new ArgumentException("Cannot make stream writable as base stream is not writable.", "write");
			
			if(seek && !stream.CanSeek)
				throw new ArgumentException("Cannot make stream seekable as base stream is not seekable.", "seek");
		}
		
		public static StreamWrapper Readonly(Stream stream) {
			return new StreamWrapper(stream, true, false, stream.CanSeek);
		}
		
		public static StreamWrapper Writeonly(Stream stream) {
			return new StreamWrapper(stream, false, true, stream.CanSeek);
		}
		
		#region Inherited
		
		public override void Write(byte[] buffer, int offset, int count) {
			if(!write)
				throw new NotSupportedException();
			
			stream.Write(buffer, offset, count);
		}
		
		public override void SetLength(long value) {
			if(!write || !seek)
				throw new NotSupportedException();
			
			stream.SetLength(value);
		}
		
		public override long Seek(long offset, SeekOrigin origin) {
			if(!seek)
				throw new NotSupportedException();
			
			return stream.Seek(offset, origin);
		}
		
		public override int Read(byte[] buffer, int offset, int count) {
			if(!read)
				throw new NotSupportedException();
			
			return stream.Read(buffer, offset, count);
		}
		
		public override long Position {
			get {
				if(!seek)
					throw new NotSupportedException();
				
				return stream.Position;
			}
			set {
				if(!seek)
					throw new NotSupportedException();
				
				stream.Position = value;
			}
		}
		
		public override long Length {
			get {
				if(!seek)
					throw new NotSupportedException();
				
				return stream.Length;
			}
		}
		
		public override void Flush() {
			stream.Flush();
		}
		
		public override bool CanWrite {
			get { return write; }
		}
		
		public override bool CanSeek {
			get { return seek; }
		}
		
		public override bool CanRead {
			get { return read; }
		}
		
		#endregion
	}
}