// involves two copies
using(var ms = new MemoryStream()) {
	sourceStream.CopyTo(ms);
	return ms.ToArray();
}

// single copy, but might contain trailing empty bytes
using(var ms = new MemoryStream()) {
	sourceStream.CopyTo(ms);
	return ms.GetBuffer();
}