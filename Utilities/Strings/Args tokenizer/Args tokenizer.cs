public static string[] SplitArguments(string args) {
	// http://stackoverflow.com/a/19725880/1377267
	
	char[] parmChars = args.ToCharArray();
	bool inSingleQuote = false;
	bool inDoubleQuote = false;
	bool escaped = false;
	bool lastSplitted = false;
	bool justSplitted = false;
	bool lastQuoted = false;
	bool justQuoted = false;
	
	int i, j;
	
	for(i=0, j=0; i<parmChars.Length; i++, j++) {
		parmChars[j] = parmChars[i];
		
		if(!escaped) {
			if(parmChars[i] == '^') {
				escaped = true;
				j--;
			} else if(parmChars[i] == '"' && !inSingleQuote) {
				inDoubleQuote = !inDoubleQuote;
				parmChars[j] = '\n';
				justSplitted = true;
				justQuoted = true;
			} else if(parmChars[i] == '\'' && !inDoubleQuote) {
				inSingleQuote = !inSingleQuote;
				parmChars[j] = '\n';
				justSplitted = true;
				justQuoted = true;
			} else if(!inSingleQuote && !inDoubleQuote && parmChars[i] == ' ') {
				parmChars[j] = '\n';
				justSplitted = true;
			}
			
			if(justSplitted && lastSplitted && (!lastQuoted || !justQuoted))
				j--;
			
			lastSplitted = justSplitted;
			justSplitted = false;
			
			lastQuoted = justQuoted;
			justQuoted = false;
		} else {
			escaped = false;
		}
	}
	
	if(lastQuoted)
		j--;
	
	return (new string(parmChars, 0, j)).Split(new[] { '\n' });
}