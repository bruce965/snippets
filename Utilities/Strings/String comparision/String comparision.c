unsigned char equalStrings(const char* string1, const char* string2)
{
	char *pointer1, *pointer2;
	
	pointer1 = (char*) string1;
	pointer2 = (char*) string2;
	
	while(*pointer1 == *pointer2)
	{
		if(*pointer1 == 0 && *pointer2 == 0)
			return 1;
		
		pointer1++;
		pointer2++;
	}
	
	return 0;
}
