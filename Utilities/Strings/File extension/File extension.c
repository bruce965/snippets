const char* getExtension(const char* filename)
{
	char *size;

	size = (char*) filename;

	while(*size)
		size++;

	while( *size != '.' )
	{
		size--;
		
		if(size == filename)
			return NULL;
	}

	return size+1;
}
