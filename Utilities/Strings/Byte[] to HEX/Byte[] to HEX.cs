public static string HexString(byte[] array) {
	int doubleSize = array.Length * 2;
	char[] result = new char[doubleSize];
	byte b;
	
	for(int x=0, y=0; y<doubleSize; x++) {
		b = ((byte)(array [x] >> 4));
		result[y++] = (char)(b > 9 ? b + 0x37 : b + 0x30);
		
		b = ((byte)(array [x] & 0xF));
		result[y++] = (char)(b > 9 ? b + 0x37 : b + 0x30);
	}
	
	return new string (result);
}