public static class String
{
	private static readonly Random _RAND = new Random();
	
	public static string RandomAlphanumString(int length) {
		const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		char[] stringChars = new char[length];
		
		for (int i = 0; i < length; i++)
			stringChars[i] = chars[_RAND.Next(chars.Length)];
		
		return new string(stringChars);
	}
}