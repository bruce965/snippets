// Convert a number to string (Italian)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

const char* ZERO     = "zero";
const char* DIECI    = "dieci";
const char* DICI     = "dici";
const char* CENTO    = "cento";
const char* MILLE    = "mille";
const char* MILA     = "mila";
const char* MILIONE  = "un milione";
const char* MILIONI  = " milioni";
const char* MILIARDO = "un miliardo";
const char* MILIARDI = " miliardi";
const char* E        = " e ";
const char* COMMA    = ", ";
const char* NEG      = " sotto zero";

const char* NUM_UNI[] = {"", "uno", "due", "tre", "quattro", "cinque", "sei", "sette", "otto", "nove"};
const char* NUM_SDC[] = {"", "un", "do", "tre", "quattor", "quin", "se", "assette", "otto", "annove"};
const char* NUM_DEC[] = {"", "", "vent", "trent", "quarant", "cinquant", "sessant", "settant", "ottant", "novant"};
const char* NUM_EXT[] = {"", "ia", "i", "a", "a", "a", "a", "a", "a", "a"};

// Range: -2^31-1, +2^31-1
char* iToString(int number)
{
	unsigned char negative = 0;
	
	if(number < 0)
	{
		negative = 1;
		number *= -1;
	}
		
	int units = (number/1         ) % 10;
	int decs  = (number/10        ) % 10;
	int cents = (number/100       ) % 10;
	int mils  = (number/1000      ) % 1000;
	int mlns  = (number/1000000   ) % 1000;
	int mlds  = (number/1000000000) % 1000;
	
	char* milString = mils > 1 ? iToString(mils) : NULL;
	char* mlnString = mlns > 1 ? iToString(mlns) : NULL;
	char* mldString = mlds > 1 ? iToString(mlds) : NULL;
	
	int length = 0;
	
	if(mlds >= 1)
	{
		if(mlds != 1)
			length += strlen(mldString) + strlen(MILIARDI);
		else
			length += strlen(MILIARDO);
	
		if(number%1000000000 > 0)
		{
			if(number%1000000 > 0)
				length += strlen(COMMA);
			else
				length += strlen(E);
		}
	}
	
	if(mlns >= 1)
	{
		if(mlns != 1)
			length += strlen(mlnString) + strlen(MILIONI);
		else
			length += strlen(MILIONE);
	
		if((number/1000)%1000 > 0)
			length += strlen(E);
	}
	
	if(mils >= 1)
	{
		if(mils != 1)
			length += strlen(milString) + strlen(MILA);
		else
			length += strlen(MILLE);
	}
	
	if(cents >= 1)
	{
		if(cents != 1)
			length += strlen(NUM_UNI[cents]);
		
		length += strlen(CENTO);
	}
	
	if(decs >= 2)
	{
		length += strlen(NUM_DEC[decs]);
		
		if(units != 1 && units != 8)
			length += strlen(NUM_EXT[decs]);
	}
	
	if(number == 0)
	{
		length = strlen(ZERO);
	}
	else if(decs != 1)
	{
		length += strlen(NUM_UNI[units]);
	}
	else if(decs == 1)
	{
		if(units == 0)
			length += strlen(DIECI);
		else if(units < 7)
			length += strlen(NUM_SDC[units]) + strlen(DICI);
		else
			length += strlen(DICI) + strlen(NUM_SDC[units]);
	}
	
	if(negative)
		length += strlen(NEG);
	
	char* string = (char*) malloc((length+1) * sizeof(char));
	
	char* pos = string;
	
	if(mlds >= 1)
	{
		if(mlds != 1)
		{
			strcpy(pos, mldString);
			pos += strlen(mldString);
			
			strcpy(pos, MILIARDI);
			pos += strlen(MILIARDI);
		}
		else
		{
			strcpy(pos, MILIARDO);
			pos += strlen(MILIARDO);
		}
			
		if(number%1000000000 > 0)
		{
			if((number/1000)%1000 > 0)
			{
				strcpy(pos, COMMA);
				pos += strlen(COMMA);
			}
			else
			{
				strcpy(pos, E);
				pos += strlen(E);
			}
		}
	}
	
	if(mlns >= 1)
	{
		if(mlns != 1)
		{
			strcpy(pos, mlnString);
			pos += strlen(mlnString);
			
			strcpy(pos, MILIONI);
			pos += strlen(MILIONI);
		}
		else
		{
			strcpy(pos, MILIONE);
			pos += strlen(MILIONE);
		}
		
		if(number%1000000 > 0)
		{
			strcpy(pos, E);
			pos += strlen(E);
		}
	}
	
	if(mils >= 1)
	{
		if(mils != 1)
		{
			strcpy(pos, milString);
			pos += strlen(milString);
			
			strcpy(pos, MILA);
			pos += strlen(MILA);
		}
		else
		{
			strcpy(pos, MILLE);
			pos += strlen(MILLE);
		}
	}
	
	if(cents >= 1)
	{
		if(cents != 1)
		{
			strcpy(pos, NUM_UNI[cents]);
			pos += strlen(NUM_UNI[cents]);
		}
		
		strcpy(pos, CENTO);
		pos += strlen(CENTO);
	}
	
	if(decs >= 2)
	{
		strcpy(pos, NUM_DEC[decs]);
		pos += strlen(NUM_DEC[decs]);
		
		if(units != 1 && units != 8)
		{
			strcpy(pos, NUM_EXT[decs]);
			pos += strlen(NUM_EXT[decs]);
		}
	}
	
	if(number == 0)
	{
		strcpy(pos, ZERO);
		//pos += strlen(ZERO);
	}
	else if(decs != 1)
	{
		strcpy(pos, NUM_UNI[units]);
		pos += strlen(NUM_UNI[units]);
	}
	else if(decs == 1)
	{
		if(units == 0)
		{
			strcpy(pos, DIECI);
			pos += strlen(DIECI);
		}
		else if(units < 7)
		{
			strcpy(pos, NUM_SDC[units]);
			pos += strlen(NUM_SDC[units]);
			strcpy(pos, DICI);
			pos += strlen(DICI);
		}
		else
		{
			strcpy(pos, DICI);
			pos += strlen(DICI);
			strcpy(pos, NUM_SDC[units]);
			pos += strlen(NUM_SDC[units]);
		}
	}
	
	if(negative)
	{
		strcpy(pos, NEG);
		// pos += strlen(NEG);
	}
	
	if(milString != NULL)
		free(milString);
	
	if(mlnString != NULL)
		free(mlnString);
	
	if(mldString != NULL)
		free(mldString);
	
	return string;
}

int main(void)
{
	while(1)
	{
		int num;
		
		printf("Inserire un numero: ");
		
		if(scanf("%d", &num) != 1)
		{
			printf("Input non valido!\n");
			fflush(stdin);
			continue;
		}
		
		char* str = iToString(num);
		
		printf("%d: %s\n", num, str);
		
		free(str);
		
		printf("\n");
	}
	
	return 0;
}
