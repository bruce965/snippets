using System;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;

namespace Utility
{
	public class MailClient
	{
		private string _username;
		private SmtpClient _client;
		
		public MailClient(string host, ushort port, string username, string password = "") {
			// NOTE: It is NOT necessary to re-estabilish a connection if the previous one is lost!
			_client = new SmtpClient(host, port);
			_client.EnableSsl = true;
			_client.UseDefaultCredentials = false;
			_client.Credentials = new NetworkCredential(username, password);
			
			ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors) { return true; };
		}
		
		public void Send(string destination, string body, string subject = "Untitled", string sender = _username) {
			_client.SendAsync(sender, to, subject, body, null);
		}
	}
}
