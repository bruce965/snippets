using System;
using System.Net;

namespace Utility
{
	public class WebClientEx : WebClient {
		
		public CookieContainer Cookies { get; set; }
		
		private Uri _previousAddress;
		
		public WebClientEx() : base() {
			this._previousAddress = new Uri("http://localhost/");
			
			this.Cookies = new CookieContainer();
		}
		
		protected override WebRequest GetWebRequest(Uri address) {
			HttpWebRequest webrequest = base.GetWebRequest(address) as HttpWebRequest;
			
			if(webrequest != null) {
				this.Cookies.SetCookies(address, this.Cookies.GetCookieHeader(this._previousAddress));
				webrequest.CookieContainer = this.Cookies;
				this._previousAddress = address;
			}
			
			return webrequest;
		}
	}
}
