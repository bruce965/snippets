// Only supports Object, Array, Date, String, Number, or Boolean.
// http://stackoverflow.com/a/728694/1377267
var clone = function(obj) {
	// Handle the 3 simple types, and null or undefined
	if(obj == null || typeof(obj) != 'object')
		return obj;

	// Handle Date
	if(obj instanceof Date) {
		var copy = new Date();
		copy.setTime(obj.getTime());
		return copy;
	}

	// Handle Array
	if(obj instanceof Array) {
		var copy = [];
		for(var i = 0, len = obj.length; i < len; i++)
			copy[i] = clone(obj[i]);
		return copy;
	}

	// Handle Object
	if(obj instanceof Object) {
		var copy = {};
		for(var attr in obj)
			if(obj.hasOwnProperty(attr))
				copy[attr] = clone(obj[attr]);
		return copy;
	}

	// Return the same object, hoping it's constant...
	console.log("Unable to copy obj! Its type isn't supported.");
	return obj;
}