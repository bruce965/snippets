MyObjectType myObject = new MyObjectType();

// Serialization
IFormatter formatter = new BinaryFormatter();
Stream stream = new FileStream("serialization.bin", FileMode.Create, FileAccess.Write, FileShare.None);
formatter.Serialize(stream, myObject);
stream.Close();

// Deserialization
IFormatter formatter = new BinaryFormatter();
Stream stream = new FileStream("serialization.bin", FileMode.Open, FileAccess.Read, FileShare.Read);
myObject = (MyObjectType) formatter.Deserialize(stream);
stream.Close();
