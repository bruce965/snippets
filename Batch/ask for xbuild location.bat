@Setlocal EnableDelayedExpansion

:: -----------------------------
:: --- ASK FOR MONO LOCATION ---
:: -----------------------------

@set whereismsg=Please select MONO installation path...
@set pickmonomsg=Please enter MONO installation path...

@if exist "%~dp0/xbuild.userconf.bat" @(
	call "%~dp0/xbuild.userconf.bat">nul
	goto checkmonopath
)

:monopathselection
@if exist "%~dp0/xbuild.userconf.bat" @del "%~dp0/xbuild.userconf.bat"

@for %%I in (powershell.exe) do @if not "%%~$PATH:I"=="" (
    for /f "delims=" %%I in ('%%~$PATH:I -sta "Add-Type -AssemblyName System.windows.forms|Out-Null;$f=New-Object System.Windows.Forms.FolderBrowserDialog;$f.Description='%whereismsg:"=\"%';$f.ShowNewFolderButton=$false;$f.ShowDialog();$f.SelectedPath"') do @(
		if "%%I"=="Cancel" goto fail
		set whereismsg=Please select MONO installation path, selected path does not contain a valid MONO installation!
		set monopath=%%I/bin/xbuild.bat
		call :GetFullPath "!monopath!"
	)
) else (
	echo !pickmonomsg!
	set pickmonomsg=This is not a valid MONO installation, try again...
	set /P monopath=^> 
	call :GetFullPath "!monopath:"=!"
	set monopath=!monopath!/bin/xbuild.bat
	call :GetFullPath "!monopath!"
)

:checkmonopath
::@echo %monopath%
@if not exist "%monopath%" @goto monopathselection
@if not exist "%~dp0/xbuild.userconf.bat" @echo set monopath=%monopath%>"%~dp0/xbuild.userconf.bat"
@goto runxbuild



:: ------------------
:: --- RUN XBUILD ---
:: ------------------

:runxbuild
@"%monopath%" %*





:GetFullPath
@set monopath=%~dpnx1
@goto :eof

:fail
@exit /b 1
