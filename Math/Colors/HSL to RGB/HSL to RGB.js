// H: 0~360   S: 0~100   L: 0~100
// R: 0~255   G: 0~255   B: 0~255
function hsl2rgb(h, s, l) {
	function hue2rgb(m1, m2, hue) {
		if(hue < 0)
			hue += 1;
		else if(hue > 1)
			hue -= 1;
		
		var v;
		
		if(6 * hue < 1)
			v = m1 + (m2 - m1) * hue * 6;
		else if(2 * hue < 1)
			v = m2;
		else if(3 * hue < 2)
			v = m1 + (m2 - m1) * (2/3 - hue) * 6;
		else
			v = m1;
		
		return 255 * v;
	}
	
	l /= 100;
	
	var m1, m2, hue;
	var r, g, b;
	
	if(s == 0) {
		r = g = b = (l * 255);
	} else {
		s /= 100;
		
		if(l <= 0.5)
			m2 = l * (s + 1);
		else
			m2 = l + s - l * s;
		
		m1 = l * 2 - m2;
		hue = h / 360;
		
		r = hue2rgb(m1, m2, hue + 1/3);
		g = hue2rgb(m1, m2, hue);
		b = hue2rgb(m1, m2, hue - 1/3);
	}
	
	return {
		r: r,
		g: g,
		b: b
	};
}
