// Precise (slow)
function distance(x1, y1, x2, y2) {
	return Math.sqrt( (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) );
}

// Approximate (-6% ~ +3%)
// http://books.google.it/books?id=_YwXHFAUl7oC&pg=PT51
function distance(x1, y1, x2, y2) {
	var dx = x1 > x2 ? x1-x2 : x2-x1;
	var dy = y1 > y2 ? y1-y2 : y2-y1;
	
	if(dx < dy)
		return 0.41*dx + 0.941246*dy;
	else
		return 0.941246*dx + 0.41*dy;
}
