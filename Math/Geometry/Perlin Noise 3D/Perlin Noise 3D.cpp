// FILE "perlin.h"

class PerlinNoise3D
{
private:
    int permutation[256];
    int* p;

    void initNoiseFunctions(bool full);
    float noise(float x, float y, float z);
    void init();
    static float fade(float t);
    static float lerp(float alpha, float a, float b);
    static float grad(int hashCode, float x, float y, float z);

public:
    float frequency;
    float amplitude;
    float persistence;
    int octaves;

    PerlinNoise3D();
    PerlinNoise3D(int permutations[256]);

    ~PerlinNoise3D();

    const int* getPermutations();
    float compute(float x, float y, float z);
};

// FILE "perlin.cpp"

#include <stdlib.h>
#include <math.h>

#include "perlin.h"

void PerlinNoise3D::initNoiseFunctions(bool full)
{
    if(full)
    {
        // Fill empty
        for (int i = 0; i < 256; i++)         // (256 is permutations number)
        {
            permutation[i] = -1;
        }
        
        // Generate random numbers
        for (int i = 0; i < 256; i++)         // (256 is permutations number)
        {
            while (true)
            {
                int iP = (rand()%256);        // (256 is permutations number)
                if (permutation[iP] == -1)
                {
                    permutation[iP] = i;
                    break;
                }
            }
        }
    }
    
    // Copy
    for (int i = 0; i < 256; i++)             // (256 is permutations number)
    {
        p[256 + i] = p[i] = permutation[i];   // (256 is permutations number)
    }
}

float PerlinNoise3D::noise(float x, float y, float z)
{
    // Find unit cube that contains point
    int iX = (int)floor(x) & 255;
    int iY = (int)floor(y) & 255;
    int iZ = (int)floor(z) & 255;
    
    // Find relative x, y, z of the point in the cube.
    x -= (float)floor(x);
    y -= (float)floor(y);
    z -= (float)floor(z);
    
    // Compute fade curves for each of x, y, z
    float u = fade(x);
    float v = fade(y);
    float w = fade(z);
    
    // Hash coordinates of the 8 cube corners
    int A = p[iX] + iY;
    int AA = p[A] + iZ;
    int AB = p[A + 1] + iZ;
    int B = p[iX + 1] + iY;
    int BA = p[B] + iZ;
    int BB = p[B + 1] + iZ;
    
    // And add blended results from 8 corners of cube.
    return lerp(w, lerp(v, lerp(u, grad(p[AA], x, y, z),
                       grad(p[BA], x - 1, y, z)),
               lerp(u, grad(p[AB], x, y - 1, z),
                       grad(p[BB], x - 1, y - 1, z))),
       lerp(v, lerp(u, grad(p[AA + 1], x, y, z - 1),
                       grad(p[BA + 1], x - 1, y, z - 1)),
               lerp(u, grad(p[AB + 1], x, y - 1, z - 1),
                       grad(p[BB + 1], x - 1, y - 1, z - 1))));
}

void PerlinNoise3D::init()
{
    frequency = 0.1f;
    amplitude = 1.0f;
    persistence = 0.5f;
    octaves = 10;
}

float PerlinNoise3D::fade(float t)
{
    // Smooth interpolation parameter
    return (t * t * t * (t * (t * 6 - 15) + 10));
}

float PerlinNoise3D::lerp(float alpha, float a, float b)
{
    // Linear interpolation
    return (a + alpha * (b - a));
}

float PerlinNoise3D::grad(int hashCode, float x, float y, float z)
{
    // Convert lower 4 bits of hash code into 12 gradient directions
    int h = hashCode & 15;
    float u = h < 8 ? x : y;
    float v = h < 4 ? y : h == 12 || h == 14 ? x : z;
    return (((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v));
}

// Simple constructor
PerlinNoise3D::PerlinNoise3D()
{
    init();
    p = new int[256 * 2];                     // (256 is permutations number)
    initNoiseFunctions(true);
}

// Construct from an array of permutations
PerlinNoise3D::PerlinNoise3D(int permutations[256])
{
    init();
    
    for(unsigned int i=0; i<256; i++)         // (256 is permutations number)
    {
        permutation[i] = permutation[i];
    }
    
    p = new int[256 * 2];                     // (256 is permutations number)
    initNoiseFunctions(false);
}

PerlinNoise3D::~PerlinNoise3D()
{
    delete p;
}

const int* PerlinNoise3D::getPermutations()
{
    return permutation;
}

float PerlinNoise3D::compute(float x, float y, float z)
{
    float noisev = 0;
    float amp = amplitude;
    float freq = frequency;
    
    for (int i = 0; i < octaves; i++)
    {
        noisev += noise(x * freq, y * freq, z * freq) * amp;
        freq  *= 2;                           // octave is the double of the previous frequency
        amp   *= persistence;
    }
    
    // Clamp and return the result
    if (noisev < 0)
    {
        return 0;
    }
    else if (noisev > 1)
    {
        return 1;
    }
    
    return noisev;
}
