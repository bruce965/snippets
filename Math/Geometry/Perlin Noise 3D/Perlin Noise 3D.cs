public class PerlinNoise3D
{
    public float Frequency = 0.1f;
    public float Amplitude = 1.0f;
    public float Persistence = 0.5f;
    public int Octaves = 10;
    
    private int[] permutation = new int[256];
    private int[] p;
    
    public PerlinNoise3D() {
        p = new int[permutation.Length * 2];
        InitNoiseFunctions(true);
    }
    
    public PerlinNoise3D(int[] permutations) {
        this.permutation = permutations;
        p = new int[permutation.Length * 2];
        InitNoiseFunctions(false);
    }
    
    public int[] GetPermutations() {
        return (int[]) permutation.Clone();
    }
    
    private void InitNoiseFunctions(bool full) {
        if(full) {
            // Fill empty
            for (int i = 0; i < permutation.Length; i++) {
                permutation[i] = -1;
            }
            
            // Generate random numbers
            for (int i = 0; i < permutation.Length; i++) {
                while (true)
                {
                    int iP = (Random.Range(0, int.MaxValue) % permutation.Length);
                    if (permutation[iP] == -1) {
                        permutation[iP] = i;
                        break;
                    }
                }
            }
        }
        
        // Copy
        for (int i = 0; i < permutation.Length; i++) {
            p[permutation.Length + i] = p[i] = permutation[i];
        }
    }

    public float Compute(float x, float y, float z) {
        float noise = 0;
        float amp = this.Amplitude;
        float freq = this.Frequency;
        
        for (int i = 0; i < this.Octaves; i++) {
            noise += Noise(x * freq, y * freq, z * freq) * amp;
            freq  *= 2;                                // octave is the double of the previous frequency
            amp   *= this.Persistence;
        }
        
        // Clamp and return the result
        if (noise < 0) {
            return 0;
        } else if (noise > 1) {
            return 1;
        }
        
        return noise;
    }

    private float Noise(float x, float y, float z) {
        // Find unit cube that contains point
        int iX = (int)Mathf.Floor(x) & 255;
        int iY = (int)Mathf.Floor(y) & 255;
        int iZ = (int)Mathf.Floor(z) & 255;
        
        // Find relative x, y, z of the point in the cube.
        x -= (float)Mathf.Floor(x);
        y -= (float)Mathf.Floor(y);
        z -= (float)Mathf.Floor(z);
        
        // Compute fade curves for each of x, y, z
        float u = Fade(x);
        float v = Fade(y);
        float w = Fade(z);
        
        // Hash coordinates of the 8 cube corners
        int A = p[iX] + iY;
        int AA = p[A] + iZ;
        int AB = p[A + 1] + iZ;
        int B = p[iX + 1] + iY;
        int BA = p[B] + iZ;
        int BB = p[B + 1] + iZ;
        
        // And add blended results from 8 corners of cube.
        return Lerp(w, Lerp(v, Lerp(u, Grad(p[AA], x, y, z),
                           Grad(p[BA], x - 1, y, z)),
                   Lerp(u, Grad(p[AB], x, y - 1, z),
                           Grad(p[BB], x - 1, y - 1, z))),
           Lerp(v, Lerp(u, Grad(p[AA + 1], x, y, z - 1),
                           Grad(p[BA + 1], x - 1, y, z - 1)),
                   Lerp(u, Grad(p[AB + 1], x, y - 1, z - 1),
                           Grad(p[BB + 1], x - 1, y - 1, z - 1))));
    }

    private static float Fade(float t) {
        // Smooth interpolation parameter
        return (t * t * t * (t * (t * 6 - 15) + 10));
    }

    private static float Lerp(float alpha, float a, float b) {
        // Linear interpolation
        return (a + alpha * (b - a));
    }

    private static float Grad(int hashCode, float x, float y, float z) {
        // Convert lower 4 bits of hash code into 12 gradient directions
        int h = hashCode & 15;
        float u = h < 8 ? x : y;
        float v = h < 4 ? y : h == 12 || h == 14 ? x : z;
        return (((h & 1) == 0 ? u : -u) + ((h & 2) == 0 ? v : -v));
    }
}
